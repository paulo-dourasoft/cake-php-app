<?php
use Migrations\AbstractSeed;

/**
 * Users seed.
 */
class UsersSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => 1,
                'name' => 'paulo 5',
                'email' => 'paulo5@dourasoft.com.br',
                'phone' => '(99) 99999-9999',
				'created' => '2017-05-29 12:02:30-03',
                'modified' => '2017-05-30 10:00:16-03',
            ],
            [
                'id' => 2,
                'name' => 'paulo 6',
                'email' => 'paulo6@dourasoft.com.br',
                'phone' => '(99) 99999-8888',
				'created' => '2017-05-29 12:02:30-03',
                'modified' => '2017-05-30 10:00:16-03',
            ],
		];

        $table = $this->table('users');
        $table->insert($data)->save();
    }
}
